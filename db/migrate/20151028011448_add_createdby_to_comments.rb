class AddCreatedbyToComments < ActiveRecord::Migration
  def change
    add_column :comments, :createdby, :string
  end
end
